const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose
	.connect(process.env.MONGODB_URI)
	.then(() => {
		console.log(`connected to ${process.env.MONGODB_URI}`);
	})
	.catch(e =>
		console.log(`Unable to connect to ${process.env.MONGODB_URI}`)
	);

module.exports = { mongoose };
