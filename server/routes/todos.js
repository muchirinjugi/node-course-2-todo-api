//LIBRARY IMPORTS
const _ = require('lodash');
const { ObjectID } = require('mongodb');
var express = require('express');
var router = express.Router();

const { mongoose } = require('./../db/mongoose');
const { Todo } = require('./../models/todo');
const { User } = require('./../models/user');
const { authenticate } = require('./../middleware/authenticate');
/**
 * @description POST/ADD TODO ROUTE
 */

router.post('/', authenticate, (req, res) => {
	var todo = new Todo({
		text: req.body.text,
		_creator: req.user._id
	});
	todo
		.save()
		.then(doc => res.send(doc))
		.catch(e => res.status(400).send(e));
});

/**
 * @description GET ALL TODOS ROUTE
 */

router.get('/', authenticate, (req, res) => {
	Todo.find({
		_creator: req.user._id
	})
		.then(todos => {
			res.send({ todos });
		})
		.catch(e => {
			res.status(400).send(e);
		});
});

/**
 * @description GET TODO BY ID ROUTE
 */

router.get('/:id', authenticate, (req, res) => {
	var id = req.params.id;

	if (!ObjectID.isValid(id)) {
		res.status(404).send();
	}

	Todo.findOne({ _id: id, _creator: req.user._id })
		.then(todo => {
			if (!todo) {
				return res.status(404).send();
			}
			res.send({ todo });
		})
		.catch(e => res.status(400).send());
});

/**
 * @description DELETE TODO BY ID ROUTE
 */

router.delete('/:id', authenticate, (req, res) => {
	var id = req.params.id;

	if (!ObjectID.isValid(id)) {
		return res.status(404).send();
	}

	Todo.findOneAndRemove({
		_id: id,
		_creator: req.user._id
	})
		.then(todo => {
			if (!todo) {
				return res.status(404).send();
			}
			res.send({ todo });
		})
		.catch(e => res.status(400).send());
});

/**
 * @description UPDATE TODO ROUTE
 */

router.patch('/:id', authenticate, (req, res) => {
	var id = req.params.id;

	if (!ObjectID.isValid(id)) {
		return res.status(404).send();
	}

	body = _.pick(req.body, ['text', 'completed']);

	if (_.isBoolean(body.completed) && body.completed) {
		body.completedAt = new Date().getTime();
	} else {
		body.completed = false;
		body.completedAt = null;
	}

	Todo.findOneAndUpdate(
		{
			_id: id,
			_creator: req.user._id
		},
		{ $set: body },
		{ new: true }
	)
		.then(todo => {
			if (!todo) return res.status(404).send();
			res.send({ todo });
		})
		.catch(e => res.status(400).send());
});

module.exports = router;
