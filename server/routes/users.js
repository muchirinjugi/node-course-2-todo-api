//LIBRARY IMPORTS
const _ = require('lodash');
const { ObjectID } = require('mongodb');
var express = require('express');
var router = express.Router();

const { mongoose } = require('./../db/mongoose');
const { Todo } = require('./../models/todo');
const { User } = require('./../models/user');
const { authenticate } = require('./../middleware/authenticate');

/**
 * @description USER ROUTE
 */
router.post('/', (req, res) => {
	var body = _.pick(req.body, ['email', 'password']);
	var user = new User(body);

	user
		.save()
		.then(user => {
			return user.generateAuthToken();
			res.send(user);
		})
		.then(token => {
			res.header('x-auth', token).send(user);
		})
		.catch(e => res.status(400).send(e));
});
/**
 * @description USERS LOGIN ROUTE
 */
router.post('/login', (req, res) => {
	var body = _.pick(req.body, ['email', 'password']);

	User.findByCredentials(body.email, body.password)
		.then(user => {
			user.generateAuthToken().then(token => {
				res.header('x-auth', token).send(user);
			});
		})
		.catch(e => {
			res.status(400).send('password or email is incorrect');
		});
});

router.get('/me', authenticate, (req, res) => {
	res.send(req.user);
});

router.delete('/me/token', authenticate, (req, res) => {
	req.user
		.removeToken(req.token)
		.then(
			() => {
				res.status(200).send();
			},
			() => {
				res.status(400).send();
			}
		)
		.catch(e => {
			res.status(400).send();
		});
});

module.exports = router;
