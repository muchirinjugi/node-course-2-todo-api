require('./config/config');

//LIBRARY IMPORTS
const express = require('express');
const bodyParser = require('body-parser');
var todos = require('./routes/todos');
var users = require('./routes/users');

const port = process.env.PORT;

var app = express();

app.use(bodyParser.json());

/**
 * @description TODOS ROUTES
 */

app.use('/todos', todos);

/**
 * @description USER ROUTE
 */

app.use('/users', users);

/**
 * @description START SERVER
 */
app.listen(port, () => {
	console.log(`Started up at port ${port}`);
});

module.exports = { app };
