const { SHA256 } = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

/**
 * @description BCRYPT
 */
var password = '123abc';

bcrypt.genSalt(10, (err, salt) => {
	bcrypt.hash(password, salt, (err, hash) => {
		console.log(hash);
	});
});

var hashedPassword =
	'$2a$10$63y4zvYLE4czBLalG1JdJex0kPS2rhLZkv9e2vcNuwXoiXWJZ4gd2';
bcrypt.compare(password, hashedPassword, (err, res) => {
	console.log(res);
});

/**
 * @description JWT
 */
// var data = {
// 	id: 4
// };

// var token = jwt.sign(data, '123abc');
// console.log(token);

// var decoded = jwt.verify(token, '123abc');
// console.log('decoded', decoded);

/**
 * @description crypto
 */
// var message = 'I am user number 3';
// var hash = SHA256(message).toString();

// console.log(`message: ${message}`);
// console.log(`Hash: ${hash}`);

// var data = {
// 	id: 4
// };

// var token = {
// 	data,
// 	hash: SHA256(JSON.stringify(data) + 'somesecret').toString()
// };

// token.data = 5;
// token.hash = SHA256(JSON.stringify(data)).toString();

// var resultHash = SHA256(JSON.stringify(token.data) + 'somesecret').toString();

// resultHash === token.hash
// 	? console.log('Data was not changed')
// 	: console.log('Data was changed. Do not trust!');
