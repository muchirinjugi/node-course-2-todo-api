const { MongoClient, ObjectID } = require('mongodb');
const url = 'mongodb://localhost:27017/TodoApp';

MongoClient.connect(url, (err, client) => {
	err
		? console.log(`unable to connect to ${url}`)
		: console.log(`connected to ${url}`);
	const db = client.db('TodoApp');
	db
		.collection('Todos')
		.findOneAndUpdate(
			{
				_id: new ObjectID('5adcc1e852390d2478565637')
			},
			{
				$set: {
					completed: true
				}
			},
			{
				returnNewDocument: true,
				returnOrignal: false
			}
		)
		.then(result => console.log(result))
		.catch(err => console.log(err));
});
