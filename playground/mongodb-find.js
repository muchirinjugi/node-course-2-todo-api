const { MongoClient, ObjectID } = require('mongodb');
const url = 'mongodb://localhost:27017/TodoApp';

MongoClient.connect(url, (err, client) => {
	err
		? console.log(`cannot connect to ${url}`)
		: console.log(`connected to ${url}`);
	const db = client.db('TodoApp');
	//  QUERY TODO COLLECTION
	db
		.collection('Todos')
		.find({
			_id: new ObjectID('5adcc1e852390d2478565637')
		})
		.toArray()
		.then(docs => {
			console.log('Todos');
			console.log(JSON.stringify(docs, undefined, 2));
		})
		.catch(err => {
			console.log('unable to fetch Todos', err);
		});
	// QUERY USERS COLLECTION
	db
		.collection('Users')
		.find({
			name: 'Muchiri Njugi'
		})
		.toArray()
		.then(docs => {
			console.log('Users');
			console.log(JSON.stringify(docs, undefined, 2));
		})
		.catch(err => {
			console.log('unable to fetch Todos', err);
		});

	// client.close();
});
