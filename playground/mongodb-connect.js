const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/TodoApp';

MongoClient.connect(url, (err, client) => {
	err
		? console.log(`unable to connect to ${url}`)
		: console.log(`connected to ${url}`);

	const db = client.db('TodoApp');
	// db.collection('Todos').insertOne(
	// 	{
	// 		text: 'Something to do',
	// 		completed: false
	// 	},
	// 	(err, result) => {
	// 		err
	// 			? console.log('unable to insert todo', err)
	// 			: console.log(JSON.stringify(result.ops, undefined, 2));
	// 	}
	// );
	db.collection('Users').insertOne(
		{
			name: 'Muchiri Njugi',
			age: 21,
			location: 'Nairobi Kenya'
		},
		(err, result) => {
			err
				? console.log('unable to insert user', err)
				: console.log(JSON.stringify(result.ops, undefined, 2));
		}
	);

	client.close();
});
