const { MongoClient, ObjectID } = require('mongodb');
const url = 'mongodb://localhost:27017/TodoApp';

MongoClient.connect(url, (err, client) => {
	err
		? console.log(`unable to connect to ${url}`)
		: console.log(`connected to ${url}`);

	const db = client.db('TodoApp');

	/**
	 * @description deleteMany
	 * @description deleteOne  works exactly as delete many but only deletes the first one
	 * @description findOneAndDelete
	 */

	/**
	 * @description deleteMany
	 */
	db
		.collection('Todos')
		.deleteMany({ text: 'Eat Lunch' })
		.then(result => console.log(result))
		.catch(err => console.log('Unable to delete', err));
	/**
	 * @description deleteOne
	 */
	db
		.collection('Todos')
		.deleteOne({ text: 'Eat Lunch' })
		.then(result => console.log(result))
		.catch(err => console.log('Unable to delete', err));
	/**
	 * @description findOneAndDelete
	 */
	db
		.collection('Todos')
		.findOneAndDelete({ completed: true })
		.then(result => console.log(JSON.stringify(result, undefined, 2)))
		.catch(err => console.log('unable to delete', err));
});
