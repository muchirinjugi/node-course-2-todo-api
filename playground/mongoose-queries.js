const { ObjectID } = require('mongodb');
const { mongoose } = require('./../server/db/mongoose');
const { Todo } = require('./../server/models/todo');
const { User } = require('./../server/models/user');

var id = '5ade85a928da634c80aeb9f3';
var userId = '5ade8cf85530b6d6abdc5920';

if (!ObjectID.isValid(id)) {
	console.log('Id not valid');
}

// Todo.find({ _id: id }).then(todos => console.log('Todos: ', todos));
// Todo.findOne({ _id: id }).then(todo => console.log('Todo: ', todo));
Todo.findById(id)
	.then(todo => {
		!todo
			? console.log('Id not found')
			: console.log('Todo By Id', todo);
	})
	.catch(e => console.log(e));

User.findById(userId)
	.then(user => {
		!user
			? console.log('user not found')
			: console.log('user by id: ', user);
	})
	.catch(e => console.log(e));
